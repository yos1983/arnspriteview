//
//  ARNAppDelegate.h
//  ARNSpriteView
//
//  Created by CocoaPods on 10/26/2014.
//  Copyright (c) 2014 xxxAIRINxxx. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARNAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
