
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ARNLayout
#define COCOAPODS_POD_AVAILABLE_ARNLayout
#define COCOAPODS_VERSION_MAJOR_ARNLayout 0
#define COCOAPODS_VERSION_MINOR_ARNLayout 1
#define COCOAPODS_VERSION_PATCH_ARNLayout 2

// ARNSpriteView
#define COCOAPODS_POD_AVAILABLE_ARNSpriteView
#define COCOAPODS_VERSION_MAJOR_ARNSpriteView 0
#define COCOAPODS_VERSION_MINOR_ARNSpriteView 1
#define COCOAPODS_VERSION_PATCH_ARNSpriteView 1

// Expecta
#define COCOAPODS_POD_AVAILABLE_Expecta
#define COCOAPODS_VERSION_MAJOR_Expecta 0
#define COCOAPODS_VERSION_MINOR_Expecta 3
#define COCOAPODS_VERSION_PATCH_Expecta 1

// Expecta+Snapshots
#define COCOAPODS_POD_AVAILABLE_Expecta_Snapshots
#define COCOAPODS_VERSION_MAJOR_Expecta_Snapshots 1
#define COCOAPODS_VERSION_MINOR_Expecta_Snapshots 3
#define COCOAPODS_VERSION_PATCH_Expecta_Snapshots 0

// FBSnapshotTestCase
#define COCOAPODS_POD_AVAILABLE_FBSnapshotTestCase
#define COCOAPODS_VERSION_MAJOR_FBSnapshotTestCase 1
#define COCOAPODS_VERSION_MINOR_FBSnapshotTestCase 4
#define COCOAPODS_VERSION_PATCH_FBSnapshotTestCase 0

// Specta
#define COCOAPODS_POD_AVAILABLE_Specta
#define COCOAPODS_VERSION_MAJOR_Specta 0
#define COCOAPODS_VERSION_MINOR_Specta 2
#define COCOAPODS_VERSION_PATCH_Specta 1

