//
//  ARNBgScene.m
//  ARNSpriteView
//
//  Created by Airin on 2014/10/01.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#if !__has_feature(objc_arc)
#error This file must be compiled with ARC. Use -fobjc-arc flag (or convert project to ARC).
#endif

#import "ARNBgScene.h"

@implementation ARNBgScene

- (id)initWithSize:(CGSize)size
{
    if (self = [super initWithSize:size]) {
        self.backgroundColor = [UIColor clearColor];
        self.physicsWorld.gravity = CGVectorMake(0, 0);
    }
    return self;
}

- (void)setContentsWithBackGroundImageNamed:(NSString *)imageName
{
    SKSpriteNode *bgSprite = [SKSpriteNode spriteNodeWithTexture:[SKTexture textureWithImageNamed:imageName]];
    bgSprite.size = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height);
    bgSprite.position = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    [self addChild:bgSprite];
    
    [self runAction:[SKAction repeatActionForever:[SKAction sequence:@[[SKAction customActionWithDuration:0 actionBlock:^(SKNode *node, CGFloat elapsedTime) {
        SKNode *baseNode = [[SKNode alloc] init];
        baseNode.alpha = 0;
        [self addChild:baseNode];
        [self addParticleWithName:@"BG_Particle" position:CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2) emit:5 node:baseNode];
        [baseNode runAction:[SKAction sequence:@[[SKAction fadeAlphaBy:1 duration:1],
                                                 [SKAction fadeAlphaBy:0 duration:9],
                                                 [SKAction runBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                [baseNode removeFromParent];
            });
        }]]]];
    }],
                                                                       [SKAction waitForDuration:5]
                                                                       ]]]];
}

- (void)addParticleWithName:(NSString *)particleName position:(CGPoint)position emit:(NSUInteger)emit node:(SKNode *)node
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"ARNSpriteView" ofType:@"bundle"];
    NSBundle *bundle = [NSBundle bundleWithPath:path];
    SKEmitterNode *particleNode = [NSKeyedUnarchiver unarchiveObjectWithFile:[bundle pathForResource:particleName ofType:@"sks"]];
    particleNode.position = position;
    particleNode.numParticlesToEmit = emit;
    particleNode.particlePositionRange = CGVectorMake(self.view.frame.size.width, self.view.frame.size.height);
    
    UIImage *image;
    NSUInteger colorCount = arc4random_uniform(3);
    if (colorCount == 0) {
        image = [UIImage imageWithContentsOfFile:[bundle pathForResource:@"circle_gray"
                                                                  ofType:@"png"]];
    } else if (colorCount == 1) {
        image = [UIImage imageWithContentsOfFile:[bundle pathForResource:@"circle_pink"
                                                                  ofType:@"png"]];
    } else {
        image = [UIImage imageWithContentsOfFile:[bundle pathForResource:@"circle_white"
                                                                  ofType:@"png"]];
    }
    
    SKTexture *texture = [SKTexture textureWithImage:image];
    particleNode.particleTexture = texture;
    
    [node addChild:particleNode];
}

@end
