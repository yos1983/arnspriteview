#
# Be sure to run `pod lib lint ARNSpriteView.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "ARNSpriteView"
  s.version          = "0.1.1"
  s.summary          = "A short description of ARNSpriteView."
  # s.homepage         = "https://github.com/xxxAIRINxxx/ARNSpriteView"
  s.homepage         = "https://bitbucket.org/yos1983/arnspriteview"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "xxxAIRINxxx" => "xl1138@gmail.com" }
  # s.source           = { :git => "https://github.com/xxxAIRINxxx/ARNSpriteView.git", :tag => s.version.to_s }
  s.source           = { :git => "https://yos1983@bitbucket.org/yos1983/arnspriteview.git", :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/*'
  s.resource_bundles = {
    'ARNSpriteView' => ['Pod/Assets/*.{png,sks}']
  }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  s.frameworks = 'SpriteKit'
  s.dependency 'ARNLayout'
end
