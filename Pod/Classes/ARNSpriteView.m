//
//  ARNSpriteView.m
//  ARNSpriteView
//
//  Created by Airin on 2014/10/01.
//  Copyright (c) 2014 Airin. All rights reserved.
//

@import SpriteKit;

#import "ARNSpriteView.h"

#import <ARNLayout.h>

#import "ARNBgScene.h"

@interface ARNSpriteView ()

@property (nonatomic, strong) SKView *skView;
@property (nonatomic, strong) ARNBgScene *scene;

@property (nonatomic, copy) NSString *imageName;

@end

@implementation ARNSpriteView

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)commonInit
{
    
    __weak typeof(self) weakSelf = self;
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidEnterBackgroundNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      if (weakSelf.skView) {
                                                          [weakSelf.skView removeFromSuperview];
                                                          weakSelf.skView = nil;
                                                      }
                                                  }];
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillEnterForegroundNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *note) {
                                                      if (!weakSelf.skView) {
                                                          [weakSelf settingSKView];
                                                          [weakSelf startSceneWithBackGroundImageNamed:weakSelf.imageName];
                                                      }
                                                  }];
    [self settingSKView];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    if (!(self = [super initWithFrame:frame])) { return nil; }
    
    [self commonInit];
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (!(self = [super initWithCoder:aDecoder])) { return nil; }
    
    [self commonInit];
    
    return self;
}

- (void)settingSKView
{
    if (!self.skView) {
        self.skView                 = [[SKView alloc] initWithFrame:self.bounds];
        self.skView.backgroundColor = [UIColor clearColor];
        
        [self addSubview:self.skView];
        [self arn_allPinWithSubView:self.skView];
        
        self.scene = [ARNBgScene sceneWithSize:self.skView.bounds.size];
        self.scene.scaleMode = SKSceneScaleModeAspectFill;
        [self.skView presentScene:self.scene];
    }
}

- (void)startSceneWithBackGroundImageNamed:(NSString *)imageName
{
    if (!imageName || !imageName.length) {
        return;
    }
    self.imageName = imageName;
    
    if (self.scene) {
        [self.scene setContentsWithBackGroundImageNamed:self.imageName];
    }
}

@end
