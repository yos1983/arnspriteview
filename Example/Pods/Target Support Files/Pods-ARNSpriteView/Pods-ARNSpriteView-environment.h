
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ARNLayout
#define COCOAPODS_POD_AVAILABLE_ARNLayout
#define COCOAPODS_VERSION_MAJOR_ARNLayout 0
#define COCOAPODS_VERSION_MINOR_ARNLayout 1
#define COCOAPODS_VERSION_PATCH_ARNLayout 2

// ARNSpriteView
#define COCOAPODS_POD_AVAILABLE_ARNSpriteView
#define COCOAPODS_VERSION_MAJOR_ARNSpriteView 0
#define COCOAPODS_VERSION_MINOR_ARNSpriteView 1
#define COCOAPODS_VERSION_PATCH_ARNSpriteView 1

