# ARNSpriteView

[![CI Status](http://img.shields.io/travis/xxxAIRINxxx/ARNSpriteView.svg?style=flat)](https://travis-ci.org/xxxAIRINxxx/ARNSpriteView)
[![Version](https://img.shields.io/cocoapods/v/ARNSpriteView.svg?style=flat)](http://cocoadocs.org/docsets/ARNSpriteView)
[![License](https://img.shields.io/cocoapods/l/ARNSpriteView.svg?style=flat)](http://cocoadocs.org/docsets/ARNSpriteView)
[![Platform](https://img.shields.io/cocoapods/p/ARNSpriteView.svg?style=flat)](http://cocoadocs.org/docsets/ARNSpriteView)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

* iOS 7.0+
* ARC

## Installation

ARNSpriteView is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

    pod "ARNSpriteView"

## Dependency

[ARNLayout](https://github.com/xxxAIRINxxx/ARNLayout)

## License

ARNSpriteView is available under the MIT license. See the LICENSE file for more info.

