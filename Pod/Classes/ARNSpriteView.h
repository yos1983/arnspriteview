//
//  ARNSpriteView.h
//  ARNSpriteView
//
//  Created by Airin on 2014/10/01.
//  Copyright (c) 2014 Airin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ARNSpriteView : UIView

- (void)startSceneWithBackGroundImageNamed:(NSString *)imageName;

@end
