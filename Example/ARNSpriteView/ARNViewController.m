//
//  ARNViewController.m
//  ARNSpriteView
//
//  Created by xxxAIRINxxx on 10/26/2014.
//  Copyright (c) 2014 xxxAIRINxxx. All rights reserved.
//

#import "ARNViewController.h"

#import <ARNSpriteView.h>
#import <ARNLayout.h>

@interface ARNViewController ()

@end

@implementation ARNViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    ARNSpriteView *spriteView = [[ARNSpriteView alloc] initWithFrame:self.view.frame];
    [self.view addSubview:spriteView];
    [spriteView startSceneWithBackGroundImageNamed:@"skView_bg"];
    [self.view arn_allPinWithSubView:spriteView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
